package com.harpagans.dashboard.backend

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient

@SpringBootApplication
class App {

    @Bean
    fun webClient(): WebClient = WebClient.create()
}

fun main(args: Array<String>) {
    SpringApplication.run(App::class.java, *args)
}
