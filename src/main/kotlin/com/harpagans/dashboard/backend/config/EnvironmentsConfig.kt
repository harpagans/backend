package com.harpagans.dashboard.backend.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.net.URI
import java.nio.file.Files
import java.nio.file.Path

@Component
class EnvironmentsConfig(@Value("\${config.path}") configPath: Path) {

    private data class Environments(val environments: List<Environment>)

    final val environments: List<Environment>

    init {
        val mapper = ObjectMapper(YAMLFactory())
        mapper.registerModule(KotlinModule())

        environments = Files.newBufferedReader(configPath).use {
            mapper.readValue(it, Environments::class.java).environments
        }
    }
}

data class Service(val name: String, val address: URI)
data class Group(val name: String, val services: List<Service>)
data class Environment(val name: String, val groups: List<Group>)