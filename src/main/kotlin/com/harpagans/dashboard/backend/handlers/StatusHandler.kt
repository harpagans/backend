package com.harpagans.dashboard.backend.handlers

import com.harpagans.dashboard.backend.integration.HealthSource
import io.reactivex.BackpressureStrategy
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.bodyToServerSentEvents
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class StatusHandler(private val healthSource: HealthSource) {

    fun stream(req: ServerRequest) : Mono<ServerResponse> {
        return ok().bodyToServerSentEvents(Flux.from(healthSource.healths.toFlowable(BackpressureStrategy.LATEST)))
    }
}