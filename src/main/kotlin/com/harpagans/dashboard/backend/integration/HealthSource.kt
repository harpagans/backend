package com.harpagans.dashboard.backend.integration

import com.harpagans.dashboard.backend.config.Environment
import com.harpagans.dashboard.backend.config.EnvironmentsConfig
import com.harpagans.dashboard.backend.config.Group
import com.harpagans.dashboard.backend.config.Service
import com.harpagans.dashboard.backend.status.EnvironmentStatus
import com.harpagans.dashboard.backend.status.GroupStatus
import com.harpagans.dashboard.backend.status.ServiceStatus
import com.harpagans.dashboard.backend.status.Status
import io.reactivex.Observable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import java.io.IOException
import java.net.ConnectException
import java.util.concurrent.TimeUnit

@Component
class HealthSource(private val webClient: WebClient, private val config: EnvironmentsConfig) {

    val healths: Observable<EnvironmentStatus> = Observable.merge(Observable
            .fromIterable(config.environments)
            .map { env ->
                Observable
                        .interval(0, 60, TimeUnit.SECONDS)
                        .flatMap { getEnvironmentStatus(env) }
                        .replay(1)
                        .refCount()
            })

    private fun getEnvironmentStatus(env: Environment): Observable<EnvironmentStatus> {
        return Observable.fromIterable(env.groups)
                .flatMap { getGroupStatus(it) }
                .toList()
                .map { it.sortedBy { it.name } }
                .map { EnvironmentStatus(env.name, it) }
                .toObservable()
    }

    private fun getGroupStatus(group: Group): Observable<GroupStatus> {
        return Observable.fromIterable(group.services)
                .flatMap { getServiceStatus(it) }
                .toList()
                .map { it.sortedBy { it.name } }
                .map { GroupStatus(group.name, it) }
                .toObservable()
    }

    private fun getServiceStatus(service: Service): Observable<ServiceStatus> {
        return Observable.fromPublisher(
                webClient.get()
                        .uri(service.address)
                        .exchange()
                        .map { it.toStatus() })
                .onErrorReturn { it.toStatus() }
                .map { ServiceStatus(service.name, it) }
    }
}

private fun ClientResponse.toStatus(): Status {
    this.bodyToMono(Void::class.java)
    return when (this.statusCode()) {
        HttpStatus.OK -> Status.UP
        else -> Status.DOWN
    }
}

private fun Throwable.toStatus() =
        when (this) {
            is ConnectException -> Status.CONNECTION_REFUSED
            is IOException -> Status.CONNECTION_CLOSED_PREMATURELY
            else -> Status.UNKNOWN_ERROR
        }