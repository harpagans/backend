package com.harpagans.dashboard.backend.routing

import com.harpagans.dashboard.backend.handlers.StatusHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.router

@Configuration
class ApiRoutes(private val statusHandler: StatusHandler) {
    @Bean
    fun router() = router {
        ("/api").nest {
            ("/status" and accept(MediaType.TEXT_EVENT_STREAM)).nest {
                GET(pattern = "/", f = statusHandler::stream)
            }
        }
    }
}