package com.harpagans.dashboard.backend.status

enum class Status {
    UP,
    DOWN,
    TIME_OUT,
    CONNECTION_REFUSED,
    CONNECTION_CLOSED_PREMATURELY,
    UNKNOWN_ERROR
}

data class EnvironmentStatus(val name: String, val groups: List<GroupStatus>)
data class GroupStatus(val name: String, val services: List<ServiceStatus>)
data class ServiceStatus(val name: String, val status: Status, val version: String = "Unknown")